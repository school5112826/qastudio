import org.example.pages.CartPage;
import org.example.pages.CheckoutPage;
import org.example.pages.MainPage;
import org.example.pages.ProductPage;
import org.testng.annotations.Test;
import com.codeborne.selenide.Selenide;

public class TestScenario {
    @Test
    public void testScenario() {

        MainPage mainPage = new MainPage();
        mainPage.openPage();
        mainPage.hoverOverProduct().clickAddToCartButton();
        Selenide.sleep(1000);
        mainPage.closeCartSidePanel();

        ProductPage productPage = mainPage.goToProductPage();
        productPage.increaseQuantity(1).addToCart();

        CartPage cartPage = mainPage.goToCartPage();
        cartPage.decreaseQuantity(2);
        cartPage.isBannerDisplayed("Корзина обновлена.");
        Selenide.sleep(2000);

        CheckoutPage checkoutPage = cartPage.goToCheckout();
        checkoutPage.enterFirstName("Имя").enterLastName("Фамилия").submitOrder();
    }
}

