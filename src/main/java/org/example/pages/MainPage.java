package org.example.pages;

import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Selenide.*;

public class MainPage {

    public void openPage(){
        open("https://testqastudio.me/");
    }

    public MainPage hoverOverProduct() {
        $x("(//a[@class='woocommerce-LoopProduct-link woocommerce-loop-product__link'])[5]").shouldBe(Condition.visible);
        return this;
    }

    public MainPage clickAddToCartButton() {
        $x("//a[@href='?add-to-cart=11342']").click();
        return this;
    }

    public MainPage closeCartSidePanel() {
        $x("//span[@class='razzi-svg-icon ']/ancestor::div[@class='cart-panel-content panel-content']//a[@class='close-account-panel button-close']//span[@class='razzi-svg-icon ']").click();
        return this;
    }

    public ProductPage goToProductPage() {
        $x("//a[@href='https://testqastudio.me/product/%d0%ba%d0%bb%d0%bb%d0%b0%d1%80%d0%b8%d0%be%d0%bd-%d0%bd%d0%b8%d0%b7%d0%ba%d0%b8%d0%b9-%d1%81%d1%82%d0%be%d0%bb%d0%b8%d0%ba/']").click();
        return new ProductPage();
    }

    public CartPage goToCartPage() {
        $(".widget_shopping_cart_content").hover().$("a.button").click();
        return new CartPage();
    }
}

