package org.example.pages;

import static com.codeborne.selenide.Selenide.*;

public class ProductPage {

    public ProductPage increaseQuantity(int amount) {
        for (int i = 0; i < amount; i++) {
            $x("(//span[@class='razzi-svg-icon razzi-qty-button increase'])[1]").click();
        }
        return this;
    }

    public ProductPage addToCart() {
        $x("(//button[@name='add-to-cart'])[1]").click();
        return this;
    }
}

