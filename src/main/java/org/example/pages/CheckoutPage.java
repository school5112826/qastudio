package org.example.pages;

import static com.codeborne.selenide.Selenide.*;

public class CheckoutPage {

    public CheckoutPage enterFirstName(String firstName) {
        $("#billing_first_name").setValue(firstName);
        return this;
    }

    public CheckoutPage enterLastName(String lastName) {
        $("#billing_last_name").setValue(lastName);
        return this;
    }

    public void submitOrder() {
        $("#place_order").click();
    }
}
