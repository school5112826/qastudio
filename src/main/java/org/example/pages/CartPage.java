package org.example.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Selenide.*;

public class CartPage {
    SelenideElement decrease = $x("(//span[@class='razzi-svg-icon razzi-qty-button decrease'])[2]").click(usingJavaScript());
    public CartPage decreaseQuantity(int amount) {
        for (int i = 0; i > amount; i++) {
            decrease.click(usingJavaScript());
        }
        return this;
    }

    public boolean isBannerDisplayed(String bannerText) {
        return $x("//div[@class= 'woocommerce-message']").shouldBe(Condition.visible).has(Condition.exactText(bannerText));
    }

    public CheckoutPage goToCheckout() {
        $x("(//a[@href='https://testqastudio.me/checkout/'])[1]").click();
        return new CheckoutPage();
    }
}
